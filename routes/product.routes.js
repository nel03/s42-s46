const express = require('express')
const productController = require('../controllers/product.controllers')
const auth = require('../auth')

const router = express.Router()

// Create a new Product (Admin Only)
router.post('/', auth.verify, productController.addProduct)
// Retrieve all Product
router.get('/', productController.getAllProducts)
// Retrieve Single Product
router.get('/:productId', productController.getSingleProduct)
// Update Product information (Admin Only)
router.put('/:productId', auth.verify, productController.updateTheProduct)
// Archive Product (Admin Only)
router.put('/:productId/archive', auth.verify, productController.archiveProduct)


module.exports = router;
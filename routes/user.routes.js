const express = require('express')
const userController = require('../controllers/user.controllers')
const auth = require('../auth')

const router = express.Router()

//User Registration
router.post('/register', userController.registerUser)
//User Authentication
router.post('/login', userController.loginUser)
// retrieve user Details
router.get('/details', auth.verify, userController.getProfile)
// set user as Admin(Admin only)
router.put('/access', auth.verify,userController.updateUser)
// Change Password
router.post('/change-password', auth.verify, userController.changePassword)

module.exports = router;